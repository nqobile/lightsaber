package edu.ics.uci.nqobile;

import java.text.DecimalFormat;
import java.util.HashMap;
import android.app.Activity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.SoundPool;
import android.media.SoundPool.OnLoadCompleteListener;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;


public class MainActivity extends Activity implements SensorEventListener {

	private SensorManager sm;
    private Sensor accelerometer;
    private Sensor magneticField;
    private float[] accelerometerData;
    private float[] magneticFieldData;
    private float[] orientationData;
    private TextView azimuthDataLabel;
    private TextView pitchDataLabel;
    private TextView rollDataLabel;
    private TextView soundsLoadingLabel;
    private DecimalFormat twoPlaces = new DecimalFormat("#.##");
    private static HashMap<Integer, Integer> spMap;
    private static SoundPool sp;
    private byte lastPosition = -1; // Initialized to a sentinel value if the app has never stored a position

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
    	// Register the SensorManager and Sensor
        sm = (SensorManager) this.getSystemService(Context.SENSOR_SERVICE);
        accelerometer = sm.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        magneticField = sm.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        
        // Create sound pool
        initSounds(getApplicationContext());
        
        sp.setOnLoadCompleteListener(new OnLoadCompleteListener() {
			@Override
			public void onLoadComplete(SoundPool soundPool, int sampleId, int status) {
				// Notify user when sounds are parsed and loaded
				soundsLoadingLabel = (TextView) findViewById(edu.ics.uci.nqobile.R.id.soundsLoadingID);
		        soundsLoadingLabel.setText("");
			}
		});
    }
    
    protected void onResume() {
        super.onResume();
        sm.registerListener(this, accelerometer, 150000);
        sm.registerListener(this, magneticField, 150000);
    }
    
    protected void onPause() {
    	super.onPause();
    	sm.unregisterListener(this);
    }

	@Override
	public void onAccuracyChanged(Sensor orientationSensor, int accuracy) {
		// We chose not to implement this method. It is not needed for the scope of the assignment.
	}

	// There was a lot of confusion about how to get the orientation data from the phone by using the android
    // documentation. The Orientation Sensor had terrible roll data, so they depreciated the sensor as of
    // Android 2.2 (API 8). The new method to get the orientation data is much more confusing, and we needed
    // help understanding it. The code we have here follows the Google Android Documentation,
    // Professor Patterson's lecture slides, and the following StackOverflow post:
    // http://stackoverflow.com/questions/20339942/android-get-device-angle-by-using-getorientation-function
	@Override
	public void onSensorChanged(SensorEvent event) {
    	if(event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
    		accelerometerData = event.values;
    	}
    	if(event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD) {
    		magneticFieldData = event.values;
    	}
    	if(accelerometerData != null && magneticFieldData != null) {
    		// R is the rotation matrix and I is the inclination matrix as defined here: 
    		// http://developer.android.com/reference/android/hardware/ (continued without space on next line)
    		// SensorManager.html#getRotationMatrix(float[], float[], float[], float[])
    		float[] R = new float[9];
    		float[] I = new float[9];
    		if (SensorManager.getRotationMatrix(R, I, accelerometerData, magneticFieldData)) {
    			// This computes the actual values needed for R, the rotation matrix, and I, the inclination
    			// matrix, and stores the data in accelerometerData and magneticFieldData, respectively. The R
    			// matrix is then needed to compute the azimuth, pitch, and role values that are returned from the
    			// getOrientation method from the SensorManager class.
    			//
    			// getRotationMatrix() returns a boolean flag of whether the calculations succeeded or not.
    			orientationData = new float[3]; // [azimuth, pitch, roll]
    			SensorManager.getOrientation(R, orientationData);
    			
    			// Values are in radians. Convert to degrees.
    			float azimuth = Float.valueOf(twoPlaces.format((orientationData[0] * (180 / (float) Math.PI) + 180)));
    			float pitch = Float.valueOf(twoPlaces.format((orientationData[1] * (180 / (float) Math.PI) + 180)));
    			float roll = Float.valueOf(twoPlaces.format((orientationData[2] * (180 / (float) Math.PI) + 180)));
    			
    			updateUIDataLabels(azimuth, pitch, roll);
    			playOrientationSounds(azimuth, pitch, roll);
    		}
    		else {
    			// The calculation of R and I matrices failed.
    			Log.w("FullscreenActivity", "The calculation of R and I failed. See the error log.");
    		}
    	}
    }

	private void updateUIDataLabels(float azimuth, float pitch, float roll) {
		azimuthDataLabel = (TextView) findViewById(edu.ics.uci.nqobile.R.id.azimuthDataID);
		azimuthDataLabel.setText(String.valueOf(azimuth));
		
		pitchDataLabel = (TextView) findViewById(edu.ics.uci.nqobile.R.id.pitchDataID);
		pitchDataLabel.setText(String.valueOf(pitch));
		
		rollDataLabel = (TextView) findViewById(edu.ics.uci.nqobile.R.id.rollDataID);
		rollDataLabel.setText(String.valueOf(roll));
	}

	private void playOrientationSounds(float azimuth, float pitch, float roll) {
		if((240 < azimuth && azimuth < 310) &&
		   (160 < pitch && pitch < 190) &&
		   (160 < roll && roll < 200)) {
			// Position 1 -- No yaw, pitch, or roll in the negative or position directions.
			if(lastPosition != 1) {
				sp.play(1, 1, 1, 1, 0, 1);
				lastPosition = 1;
			}
		}
		else if((240 < azimuth && azimuth < 310) &&
		   (90 < pitch && pitch < 120) &&
		   (160 < roll && roll < 200)) {
			// Position 2 -- No yaw or roll, but the phone is tilted upwards by the top of the phone.
			if(lastPosition != 2) {
				sp.play(2, 1, 1, 1, 0, 1);
				lastPosition = 2;
			}
		}
		else if((240 < azimuth && azimuth < 310) &&
		   (210 < pitch && pitch < 240) &&
		   (160 < roll && roll < 200)) {
			// Position 3 -- No yaw or roll, but the phone is tilted downwards by the top of the phone.
			if(lastPosition != 3) {
				sp.play(3, 1, 1, 1, 0, 1);
				lastPosition = 3;
			}
		}
		else if((240 < azimuth && azimuth < 310) &&
		   (160 < pitch && pitch < 190) &&
		   (80 < roll && roll < 120)) {
			// Position 4 -- Laying flat on a level table with the phone rolled counter-clockwise (screen is
			// facing towards your left and back of the phone is facing to the right)
			if(lastPosition != 4) {
				sp.play(4, 1, 1, 1, 0, 1);
				lastPosition = 4;
			}
		}
		else if((240 < azimuth && azimuth < 310) &&
		   (160 < pitch && pitch < 190) &&
		   (245 < roll && roll < 280)) {
			// Position 5 -- Laying flat on a level table with the phone rolled clockwise (screen is
			// facing towards your right and back of the phone is facing to the left)
			if(lastPosition != 5) {
				sp.play(5, 1, 1, 1, 0, 1);
				lastPosition = 5;
			}
		}
		else {
			// Do nothing
		}
	}
	
	private static void initSounds(Context context) {
		int s1 = edu.ics.uci.nqobile.R.raw.saber_1;
		int s2 = edu.ics.uci.nqobile.R.raw.saber_2;
		int s3 = edu.ics.uci.nqobile.R.raw.saber_3;
		int s4 = edu.ics.uci.nqobile.R.raw.saber_4;
		int s5 = edu.ics.uci.nqobile.R.raw.saber_5;
		int s6 = edu.ics.uci.nqobile.R.raw.saber_6;
		
		sp = new SoundPool.Builder().build();
		spMap = new HashMap<Integer, Integer>(6);

		spMap.put(s1, sp.load(context, s1, 1));
		spMap.put(s2, sp.load(context, s2, 2));
		spMap.put(s3, sp.load(context, s3, 3));
		spMap.put(s4, sp.load(context, s4, 4));
		spMap.put(s5, sp.load(context, s5, 5));
		spMap.put(s6, sp.load(context, s6, 6));
	}
}
